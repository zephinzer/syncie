# Syncie
Syncie is a multi-platform synchronised notes application created with software engineers in mind.

Supported platforms: Linux, macOS, Windows, iOS, Android

# Features 1.0
- [ ] Truly cross-platform application
- [ ] Synchronised notes across all application instances
- [ ] Markdown compatible

# Scope of Work 1.0
- [ ] Application for Linux
- [ ] Application for macOS
- [ ] Application for Windows
- [ ] Application for iOS
- [ ] Application for Android
- [ ] Synchronization mechanism

# Technologies
## Runtimes
- JavaScript (Client application)
- Go (Server application)

## Major Frameworks/Libraries
- [Electron](https://electronjs.org/) (For desktop apps)
- [React Native](https://facebook.github.io/react-native/) (For mobile apps)
- [React](https://reactjs.org/)
- [Monaco Editor](https://microsoft.github.io/monaco-editor/)
- [Redux](https://redux.js.org/)

## Release Platforms
- [Snap](https://docs.snapcraft.io/releasing-your-app/6795) (For Linux)
- [Brew](https://docs.brew.sh/Formula-Cookbook) (For macOS)
- [Chocolatey](https://github.com/chocolatey/choco/wiki/CreatePackages) (For Windows)
- [Apple AppStore](https://developer.apple.com/app-store/submissions/) (For iOS)
- [Google PlayStore](https://developer.android.com/studio/publish/) (For Android)

# License
This project is licensed under [the MIT license](./LICENSE).
